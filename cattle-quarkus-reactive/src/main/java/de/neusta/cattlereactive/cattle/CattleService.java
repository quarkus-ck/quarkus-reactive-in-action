package de.neusta.cattlereactive.cattle;

import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.NoResultException;
import org.jboss.logging.Logger;

@ApplicationScoped
public class CattleService {

  @Inject
  CattleRepository cattleRepository;
  @Inject
  Logger log;

  @WithTransaction
  Uni<CattleDto> findCattle(Long lom) {
    return cattleRepository.find("lom", lom).singleResult()
      .onItem().transform(CattleDto::from)
      .onFailure(NoResultException.class).recoverWithNull();
  }

  @WithTransaction
  public Uni<CattleDto> createCattle(CattleDto cattleDto) {
    return Uni.createFrom().item(cattleDto).onItem().transform(CattleDto::toCattle)
      .call(cattleRepository::persist)
      .map(CattleDto::from);
  }
}
