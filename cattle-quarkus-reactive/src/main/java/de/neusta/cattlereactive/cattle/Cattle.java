package de.neusta.cattlereactive.cattle;

import de.neusta.cattlereactive.EntityBase;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Cattle extends EntityBase {

  @NotNull
  private Long lom;
  @Past
  private LocalDate birthday;
  @ManyToOne
  private Cattle dam;
  @ManyToOne
  private Cattle sire;

}
