package de.neusta.cattlereactive.cattle;

import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

@Path("cattle")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
public class CattleResource {

  @Context
  UriInfo uriInfo;
  @Inject
  CattleService cattleService;
  @Inject
  CattleRepository cattleRepository;
  @Inject
  Logger log;

  @GET
  @Path("{lom}")
  public Uni<CattleDto> byLom(Long lom) {
    return cattleService.findCattle(lom);
  }

  @GET
  @Path("/count")
  public Uni<Long> count() {
    return cattleRepository.count();
  }

  @POST
  @Path("")
  @Consumes(MediaType.APPLICATION_JSON)
  public Uni<RestResponse<Object>> create(CattleDto cattleDto) {
    return Uni.createFrom().item(cattleDto)
      .call(cattleService::createCattle)
      .map(c -> RestResponse.created(
        uriInfo.getBaseUriBuilder().path(CattleResource.class).path(CattleResource.class, "byLom").build(c.lom())))
      .onFailure().recoverWithItem(t -> RestResponse.status(RestResponse.Status.INTERNAL_SERVER_ERROR, t.getMessage()));
  }
}
