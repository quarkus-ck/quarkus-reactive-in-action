package de.neusta.cattlereactive.cattle;

import java.time.LocalDate;

public record CattleDto(
  Long lom,
  LocalDate birthday,
  Long damLom,
  Long sireLom
) {
  public static CattleDto from(Cattle cattle) {
    return new CattleDto(
      cattle.getLom(),
      cattle.getBirthday(),
      getLom(cattle.getDam()),
      getLom(cattle.getSire())
    );
  }

  private static Long getLom(Cattle cattle) {
    return cattle != null ? cattle.getLom() : null;
  }

  public Cattle toCattle() {
    Cattle cattle = new Cattle();
    cattle.setLom(lom);
    cattle.setBirthday(birthday);
    cattle.setDam(null); // todo
    cattle.setSire(null); // todo
    return cattle;
  }
}
