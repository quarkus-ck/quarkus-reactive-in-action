package de.neusta.cattlereactive.cattle;

import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CattleRepository implements PanacheRepository<Cattle> {
}
