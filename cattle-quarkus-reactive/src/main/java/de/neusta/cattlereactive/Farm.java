package de.neusta.cattlereactive;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Farm extends EntityBase{
  private String name;
}
