
Wildfly

* 1000 VUs, 20*1000 Iterations, 1 s sleep, random GET

1. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.wildfly.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 3.3 MB 42 kB/s
     data_sent......................: 1.8 MB 23 kB/s
     http_req_blocked...............: avg=3.82ms   min=1.73µs  med=4.67µs  max=235.64ms p(90)=7.76µs   p(95)=158.11µs
     http_req_connecting............: avg=3.74ms   min=0s      med=0s      max=234ms    p(90)=0s       p(95)=4.25µs  
     http_req_duration..............: avg=2.87s    min=1.2s    med=2.41s   max=7.77s    p(90)=4.4s     p(95)=4.8s    
       { expected_response:true }...: avg=2.87s    min=1.2s    med=2.41s   max=7.77s    p(90)=4.4s     p(95)=4.8s    
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=76.05µs  min=19.52µs med=61.12µs max=3.59ms   p(90)=103.77µs p(95)=151.2µs 
     http_req_sending...............: avg=788.05µs min=7.44µs  med=19.65µs max=82.87ms  p(90)=43.46µs  p(95)=266.9µs 
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=2.86s    min=1.2s    med=2.4s    max=7.76s    p(90)=4.4s     p(95)=4.8s    
     http_reqs......................: 20000  252.522131/s
     iteration_duration.............: avg=3.87s    min=2.3s    med=3.41s   max=8.96s    p(90)=5.4s     p(95)=5.8s    
     iterations.....................: 20000  252.522131/s
     vus............................: 117    min=117      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (01m19.2s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  01m19.2s/10m0s  20000/20000 shared iters  

5. Lauf

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.wildfly.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 3.3 MB 78 kB/s
     data_sent......................: 1.8 MB 42 kB/s
     http_req_blocked...............: avg=2.14ms   min=1.41µs  med=4.03µs  max=215.88ms p(90)=6.36µs  p(95)=171.32µs
     http_req_connecting............: avg=1.95ms   min=0s      med=0s      max=199.01ms p(90)=0s      p(95)=4.17µs  
     http_req_duration..............: avg=1.08s    min=45.08ms med=1.09s   max=2.1s     p(90)=1.2s    p(95)=1.28s   
       { expected_response:true }...: avg=1.08s    min=45.08ms med=1.09s   max=2.1s     p(90)=1.2s    p(95)=1.28s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=63.81µs  min=18.59µs med=49.5µs  max=3.34ms   p(90)=80.58µs p(95)=127.9µs 
     http_req_sending...............: avg=520.33µs min=6.6µs   med=16.38µs max=114.69ms p(90)=40.19µs p(95)=179.15µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=1.08s    min=43.33ms med=1.09s   max=2.1s     p(90)=1.2s    p(95)=1.28s   
     http_reqs......................: 20000  467.639926/s
     iteration_duration.............: avg=2.08s    min=1.06s   med=2.09s   max=3.21s    p(90)=2.2s    p(95)=2.28s   
     iterations.....................: 20000  467.639926/s
     vus............................: 481    min=481      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m42.8s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m42.8s/10m0s  20000/20000 shared iters           

* 1000 VUs, 20*1000 Iterations, 0 s sleep, random GET

1. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.wildfly.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 3.3 MB 47 kB/s
     data_sent......................: 1.8 MB 26 kB/s
     http_req_blocked...............: avg=3.7ms    min=1.56µs   med=3.91µs  max=243.95ms p(90)=6.17µs  p(95)=149.66µs
     http_req_connecting............: avg=3.62ms   min=0s       med=0s      max=243.86ms p(90)=0s      p(95)=4.79µs  
     http_req_duration..............: avg=3.44s    min=68.94ms  med=3.39s   max=6.1s     p(90)=4.14s   p(95)=4.98s   
       { expected_response:true }...: avg=3.44s    min=68.94ms  med=3.39s   max=6.1s     p(90)=4.14s   p(95)=4.98s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=68.35µs  min=21.25µs  med=59.3µs  max=5.85ms   p(90)=90.04µs p(95)=104.74µs
     http_req_sending...............: avg=610.04µs min=7.5µs    med=17.97µs max=73.21ms  p(90)=35.02µs p(95)=62.91µs 
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=3.44s    min=64.83ms  med=3.39s   max=6.08s    p(90)=4.13s   p(95)=4.98s   
     http_reqs......................: 20000  283.342157/s
     iteration_duration.............: avg=3.45s    min=168.16ms med=3.39s   max=6.28s    p(90)=4.18s   p(95)=4.99s   
     iterations.....................: 20000  283.342157/s
     vus............................: 252    min=252      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (01m10.6s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  01m10.6s/10m0s  20000/20000 shared iters           

5. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.wildfly.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 3.3 MB 79 kB/s
     data_sent......................: 1.8 MB 43 kB/s
     http_req_blocked...............: avg=4.88ms   min=1.5µs    med=3.42µs  max=281.57ms p(90)=5.71µs  p(95)=188.4µs 
     http_req_connecting............: avg=4.73ms   min=0s       med=0s      max=281.51ms p(90)=0s      p(95)=5.17µs  
     http_req_duration..............: avg=2.04s    min=72.8ms   med=2.09s   max=2.4s     p(90)=2.19s   p(95)=2.2s    
       { expected_response:true }...: avg=2.04s    min=72.8ms   med=2.09s   max=2.4s     p(90)=2.19s   p(95)=2.2s    
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=70.33µs  min=21.54µs  med=49.1µs  max=14.68ms  p(90)=74.03µs p(95)=86.71µs 
     http_req_sending...............: avg=731.62µs min=7.82µs   med=15.58µs max=103.44ms p(90)=29.99µs p(95)=154.08µs
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=2.04s    min=69.97ms  med=2.09s   max=2.4s     p(90)=2.19s   p(95)=2.2s    
     http_reqs......................: 20000  474.786261/s
     iteration_duration.............: avg=2.05s    min=179.33ms med=2.09s   max=2.4s     p(90)=2.19s   p(95)=2.2s    
     iterations.....................: 20000  474.786261/s
     vus............................: 138    min=138      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m42.1s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m42.1s/10m0s  20000/20000 shared iters                 

---

* Quarkus Classic 1 CPU, 512 MB



* 1000 VUs, 20*1000 Iterations, 1 s sleep, random GET

1. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qclassic.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.1 MB 45 kB/s
     data_sent......................: 1.8 MB 39 kB/s
     http_req_blocked...............: avg=3ms      min=1.37µs  med=3.46µs   max=235.01ms p(90)=6.01µs   p(95)=487.95µs
     http_req_connecting............: avg=2.9ms    min=0s      med=0s       max=234.95ms p(90)=0s       p(95)=94.45µs 
     http_req_duration..............: avg=1.29s    min=1.56ms  med=590.48ms max=10.45s   p(90)=3.19s    p(95)=4.82s   
       { expected_response:true }...: avg=1.29s    min=1.56ms  med=590.48ms max=10.45s   p(90)=3.19s    p(95)=4.82s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=52.19µs  min=14.46µs med=36.75µs  max=10.79ms  p(90)=66.06µs  p(95)=84.88µs 
     http_req_sending...............: avg=702.66µs min=5.36µs  med=15.07µs  max=200.94ms p(90)=172.89µs p(95)=812.23µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=1.29s    min=1.48ms  med=590.39ms max=10.35s   p(90)=3.19s    p(95)=4.82s   
     http_reqs......................: 20000  429.218332/s
     iteration_duration.............: avg=2.29s    min=1s      med=1.59s    max=11.55s   p(90)=4.19s    p(95)=5.82s   
     iterations.....................: 20000  429.218332/s
     vus............................: 752    min=752      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m46.6s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m46.6s/10m0s  20000/20000 shared iters   

5. Lauf

         /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qclassic.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.1 MB 100 kB/s
     data_sent......................: 1.8 MB 85 kB/s
     http_req_blocked...............: avg=5.89ms   min=1.29µs   med=3.61µs  max=290.69ms p(90)=5.86µs  p(95)=501.82µs
     http_req_connecting............: avg=5.8ms    min=0s       med=0s      max=287.36ms p(90)=0s      p(95)=149.14µs
     http_req_duration..............: avg=30.64ms  min=594.27µs med=2.35ms  max=590.12ms p(90)=44.5ms  p(95)=168.4ms 
       { expected_response:true }...: avg=30.64ms  min=594.27µs med=2.35ms  max=590.12ms p(90)=44.5ms  p(95)=168.4ms 
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=46.4µs   min=14.9µs   med=37.71µs max=5.33ms   p(90)=60.05µs p(95)=75.36µs 
     http_req_sending...............: avg=324.48µs min=6.12µs   med=15.45µs max=51.72ms  p(90)=68.6µs  p(95)=402.61µs
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=30.27ms  min=543.68µs med=2.28ms  max=586.47ms p(90)=44.42ms p(95)=149.67ms
     http_reqs......................: 20000  943.198147/s
     iteration_duration.............: avg=1.03s    min=1s       med=1s      max=1.86s    p(90)=1.04s   p(95)=1.2s    
     iterations.....................: 20000  943.198147/s
     vus............................: 564    min=564      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m21.2s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m21.2s/10m0s  20000/20000 shared iters               


* 1000 VUs, 20*1000 Iterations, 0 s sleep, random GET

1. Lauf:

         /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qclassic.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.1 MB 50 kB/s
     data_sent......................: 1.8 MB 42 kB/s
     http_req_blocked...............: avg=3.92ms   min=1.37µs   med=3.02µs  max=208.01ms p(90)=5.02µs  p(95)=217.96µs
     http_req_connecting............: avg=3.85ms   min=0s       med=0s      max=207.93ms p(90)=0s      p(95)=4.01µs  
     http_req_duration..............: avg=2.11s    min=194.88ms med=1.5s    max=16.73s   p(90)=4s      p(95)=4.93s   
       { expected_response:true }...: avg=2.11s    min=194.88ms med=1.5s    max=16.73s   p(90)=4s      p(95)=4.93s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=48.77µs  min=17.29µs  med=33.39µs max=8.06ms   p(90)=56.73µs p(95)=71.92µs 
     http_req_sending...............: avg=936.98µs min=6.77µs   med=13.77µs max=124.56ms p(90)=25.94µs p(95)=194.72µs
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=2.11s    min=193.2ms  med=1.5s    max=16.66s   p(90)=4s      p(95)=4.86s   
     http_reqs......................: 20000  468.693427/s
     iteration_duration.............: avg=2.11s    min=287.85ms med=1.5s    max=16.87s   p(90)=4s      p(95)=5.06s   
     iterations.....................: 20000  468.693427/s
     vus............................: 1000   min=1000     max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m42.7s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m42.7s/10m0s  20000/20000 shared iters        

5. Lauf

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qclassic.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.1 MB 196 kB/s
     data_sent......................: 1.8 MB 166 kB/s
     http_req_blocked...............: avg=3.15ms   min=1.28µs  med=2.85µs   max=201.97ms p(90)=4.8µs    p(95)=209.8µs 
     http_req_connecting............: avg=3.09ms   min=0s      med=0s       max=194.96ms p(90)=0s       p(95)=4.89µs  
     http_req_duration..............: avg=516.46ms min=34.81ms med=483.93ms max=2.36s    p(90)=867.41ms p(95)=1.07s   
       { expected_response:true }...: avg=516.46ms min=34.81ms med=483.93ms max=2.36s    p(90)=867.41ms p(95)=1.07s   
     http_req_failed................: 0.00%  ✓ 0           ✗ 20000 
     http_req_receiving.............: avg=64.46µs  min=16.52µs med=31.68µs  max=48ms     p(90)=50.42µs  p(95)=67.05µs 
     http_req_sending...............: avg=195.27µs min=6.42µs  med=13.25µs  max=30.49ms  p(90)=28.34µs  p(95)=205.57µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=516.2ms  min=34.75ms med=483.82ms max=2.36s    p(90)=867.34ms p(95)=1.07s   
     http_reqs......................: 20000  1848.249292/s
     iteration_duration.............: avg=519.74ms min=34.88ms med=484.59ms max=2.4s     p(90)=876.94ms p(95)=1.07s   
     iterations.....................: 20000  1848.249292/s
     vus............................: 1000   min=1000      max=1000
     vus_max........................: 1000   min=1000      max=1000


running (00m10.8s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m10.8s/10m0s  20000/20000 shared iters             


---

Quarkus reactive 1 CPU, 500 MB



* 1000 VUs, 20*1000 Iterations, 1 s sleep, random GET

1. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qreactive.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.4 MB 57 kB/s
     data_sent......................: 1.8 MB 43 kB/s
     http_req_blocked...............: avg=1.41ms  min=1.57µs   med=4.24µs   max=153.8ms  p(90)=6.54µs  p(95)=151.16µs
     http_req_connecting............: avg=1.31ms  min=0s       med=0s       max=143.44ms p(90)=0s      p(95)=4.07µs  
     http_req_duration..............: avg=1.05s   min=205ms    med=723.27ms max=6.65s    p(90)=2.09s   p(95)=3.12s   
       { expected_response:true }...: avg=1.05s   min=205ms    med=723.27ms max=6.65s    p(90)=2.09s   p(95)=3.12s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=56.55µs min=16.87µs  med=50.07µs  max=2.08ms   p(90)=80.33µs p(95)=94.54µs 
     http_req_sending...............: avg=68.42µs min=6.77µs   med=17.5µs   max=154.26ms p(90)=35.56µs p(95)=67.61µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s       max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=1.05s   min=204.88ms med=723.17ms max=6.65s    p(90)=2.09s   p(95)=3.12s   
     http_reqs......................: 20000  477.598045/s
     iteration_duration.............: avg=2.05s   min=1.2s     med=1.72s    max=7.65s    p(90)=3.09s   p(95)=4.12s   
     iterations.....................: 20000  477.598045/s
     vus............................: 31     min=31       max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m41.9s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m41.9s/10m0s  20000/20000 shared iters            

5. Lauf

         /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qreactive.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.4 MB 108 kB/s
     data_sent......................: 1.8 MB 81 kB/s
     http_req_blocked...............: avg=4.33ms   min=1.46µs   med=3.8µs   max=238.23ms p(90)=6.25µs   p(95)=214.97µs
     http_req_connecting............: avg=4.26ms   min=0s       med=0s      max=229.78ms p(90)=0s       p(95)=5.16µs  
     http_req_duration..............: avg=70.2ms   min=837.63µs med=11.67ms max=1.31s    p(90)=102.85ms p(95)=203.49ms
       { expected_response:true }...: avg=70.2ms   min=837.63µs med=11.67ms max=1.31s    p(90)=102.85ms p(95)=203.49ms
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=50.07µs  min=14.76µs  med=41.62µs max=4.01ms   p(90)=66.04µs  p(95)=80.68µs 
     http_req_sending...............: avg=287.39µs min=6.7µs    med=16.03µs max=38.23ms  p(90)=39.07µs  p(95)=275.23µs
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s      max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=69.86ms  min=790.39µs med=11.61ms max=1.31s    p(90)=102.78ms p(95)=200.42ms
     http_reqs......................: 20000  903.229802/s
     iteration_duration.............: avg=1.07s    min=1s       med=1.01s   max=2.51s    p(90)=1.1s     p(95)=1.21s   
     iterations.....................: 20000  903.229802/s
     vus............................: 283    min=283      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m22.1s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m22.1s/10m0s  20000/20000 shared iters   

* 1000 VUs, 20*1000 Iterations, 0 s sleep, random GET

1. Lauf:

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qreactive.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.4 MB 51 kB/s
     data_sent......................: 1.8 MB 38 kB/s
     http_req_blocked...............: avg=2.85ms   min=1.7µs   med=3.67µs  max=187.65ms p(90)=5.64µs  p(95)=183.16µs
     http_req_connecting............: avg=2.81ms   min=0s      med=0s      max=187.59ms p(90)=0s      p(95)=5.61µs  
     http_req_duration..............: avg=2.29s    min=1.2s    med=1.8s    max=9.49s    p(90)=3.69s   p(95)=5.19s   
       { expected_response:true }...: avg=2.29s    min=1.2s    med=1.8s    max=9.49s    p(90)=3.69s   p(95)=5.19s   
     http_req_failed................: 0.00%  ✓ 0          ✗ 20000 
     http_req_receiving.............: avg=57.63µs  min=20.59µs med=51.07µs max=17.66ms  p(90)=82.19µs p(95)=93.44µs 
     http_req_sending...............: avg=183.19µs min=7.92µs  med=17.27µs max=36.63ms  p(90)=30.07µs p(95)=78.91µs 
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s      p(95)=0s      
     http_req_waiting...............: avg=2.29s    min=1.2s    med=1.8s    max=9.48s    p(90)=3.69s   p(95)=5.19s   
     http_reqs......................: 20000  427.256017/s
     iteration_duration.............: avg=2.3s     min=1.2s    med=1.8s    max=9.49s    p(90)=3.69s   p(95)=5.19s   
     iterations.....................: 20000  427.256017/s
     vus............................: 697    min=697      max=1000
     vus_max........................: 1000   min=1000     max=1000


running (00m46.8s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
default ✓ [======================================] 1000 VUs  00m46.8s/10m0s  20000/20000 shared iters  

5. Lauf

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

     execution: local
        script: k6/script.qreactive.js
        output: -

     scenarios: (100.00%) 1 scenario, 1000 max VUs, 10m30s max duration (incl. graceful stop):
              * default: 20000 iterations shared among 1000 VUs (maxDuration: 10m0s, gracefulStop: 30s)


     data_received..................: 2.4 MB 137 kB/s
     data_sent......................: 1.8 MB 103 kB/s
     http_req_blocked...............: avg=3.89ms   min=1.6µs   med=3.34µs   max=221.91ms p(90)=5.33µs   p(95)=196.6µs 
     http_req_connecting............: avg=3.84ms   min=0s      med=0s       max=221.81ms p(90)=0s       p(95)=5.45µs  
     http_req_duration..............: avg=842.88ms min=44.06ms med=875.89ms max=1.11s    p(90)=938.99ms p(95)=989.28ms
       { expected_response:true }...: avg=842.88ms min=44.06ms med=875.89ms max=1.11s    p(90)=938.99ms p(95)=989.28ms
     http_req_failed................: 0.00%  ✓ 0           ✗ 20000 
     http_req_receiving.............: avg=51.32µs  min=21.02µs med=41.99µs  max=29.61ms  p(90)=61.74µs  p(95)=71.89µs 
     http_req_sending...............: avg=423.34µs min=7.77µs  med=15.21µs  max=110.91ms p(90)=27.66µs  p(95)=197.49µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=842.41ms min=42.54ms med=875.83ms max=1.11s    p(90)=938.94ms p(95)=989.22ms
     http_reqs......................: 20000  1140.890025/s
     iteration_duration.............: avg=846.88ms min=44.2ms  med=876.96ms max=1.11s    p(90)=943.79ms p(95)=989.46ms
     iterations.....................: 20000  1140.890025/s
     vus............................: 697    min=697       max=1000
     vus_max........................: 1000   min=1000      max=1000


running (00m17.5s), 0000/1000 VUs, 20000 complete and 0 interrupted iterations
