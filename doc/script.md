# Quarkus Reactive in Action

## TODO

* [ ] Stefans Teile einbinden
* [ ] Probevortrag -> Zeit nehmen (30..35 Minuten, damit noch Zeit für Fragen bleibt)
* [ ] Probevortrag vor Publikum (vit, neusta?) oder Video

## Vortragsskript:

-> Slide: Titel

-> Matt:
Schön, dass unser Thema euer Interesse geweckt hat.
Ich bin Matthias Bremer, seit 2011 bei neusta und seit dem auch als Software-Entwickler mit Schwerpunkt Java / Java-EE/Jakarta beim Kunden.
Vorher habe ich (im vergleichsweise fortgeschrittenen Alter) Technomathematik studiert.

-> Stefan:
(stellt sich vor)

* [ ] TODO: Vielleicht kann die Folie 'Wir' weg und wir erzählen das zur Titelfolie und zur Folie 'Warum Quarkus'

-> Matt oder Stefan:
Während der Vorbereitung der Konferenz wurden wir gefragt, um was es in unserem Vortrag geht... von einer Nicht-Entwicklerin! Das war gar nicht so leicht zu beantworten. Aber im Grunde geht es um das Java-Framework Quarkus und Programmier-Paradigma Reactive Programming.

Eine kurze Umfrage noch, damit wir wissen, was ihr mitbringt: 
(Fragen ins Publikum sind zur Auflockerung ganz gut)
Wer von euch hat schon kleinere oder größere Projekte mit Quarkus umgesetzt? 
(Handzeichen)
Wer hat schon reactive programmiert... auch ohne Quarkus? 
(Hanzeichen)

Reactive Programming ist ein integraler Bestandteil von Quarkus. Um Quarkus zu benutzen muss man nichts darüber wissen. Quarkus unterstützt sowohl die im Jakarta-Umfeld verbreitete imperative Programmierung als auch die reactive Programmierung.

Doch bevor wir zu Quarkus kommen, erst einmal eine Einführung in Reactive Programming.


-> Stefan
* [ ] TODO Folien über Reactive Programming
* [ ] TODO Stefan erzählt etwas über Reactive Programming


-> Slide: Warum Quarkus?

-> Matt oder Stefan:
Wir sind seit einigen Jahren in unterschiedlichen Projekten aber beim gleichen Kunden. Die Software dort ist backendlastig und wurde bis vor ca. 2 Jahren bevorzugt als Jakarta-Anwendung auf Wildfly-Application-Servern betrieben. 
Das ist allerdings nicht immer ganz einfach.
Quarkus wurde dann als Alternatve auserkoren und mit der Einführung von Docker-Swarm als Deployment-Plattform eingeführt.
Vorteile: Jakarta-Erfahrung kann weiter genutzt werden. Quarkus-Apps werden i.d.R. mit Quasi-Standards wie CDI - Die Quarkus-Implementierung heißt ArC, Hibernate, RestEasy, JsonB, den SmallRye Microprofile-Implementierungen gebaut.
Konfiguration, Testing und Entwicklung im Quarkus-Devmode sind flotter und leichtgewichtiger.
Anwendungen starten schnell und sind klein, also dafür dass es Java ist.

-> Slide: Historie

-> Matt:
Quarkus lässt sich am ehesten mit Spring Boot vergleichen. Spring Boot hatte das erste Release, lt. Wikipedia, 2014. Also vor 10 Jahren. 
Von Red Hat wurde im gleichen Jahr der JBoss Application Server in Wildfly umbenannt. JBoss war schon seit 2006 unter der Regie von Red Hat. 5 Jahre nach der Erfindung von Spring Boot, also 2019, hat Red Hat seine Java-EE-Erfahrung genutzt und Quarkus 1.0 released.
Danach ging es rasant mit neuen Features weiter.

-> Slide: Quarkus CLI

-> Matt:
Nun möchte ich kurz die herausagenden Features von Quarkus zeigen, um anschließend wieder zum Reactive Programming einzuschwänken.
Quarkus CLI: Quarkus lässt sich komplett mit einem Maven-Plugin bedienen. Ich bevorzuge jedoch das CLI. Man kann:
(alles auf der Commandline kurz zeigen - copy&paste - Achtung: Vorher einmal ausführen, damit die Dependencies lokal liegen)
* eine App from cratch erstellen
* im Dev-Mode starten
(dev-ui zeigen http://localhost:8080/q/dev-ui, zu Endpoints gehen)
(r drücken und continous testing zeigen)
(src/main/java/de/neusta/qdemo/GreetingResource.java editieren)
Das sieht sehr nach einer Jakarta-App aus.
(test anpassen src/test/java/de/neusta/qdemo/GreetingResourceTest.java editieren)
Hier zeigt sich schon die Test-Magie von Quarkus. Wir brauchen den Test einfach nur mit @QuarkusTest annotieren, benutzen JUnit5 und RestAssured und müssen sonst nichts weiter tun. Um das Hochfahren der App kümmert sich Quarkus. 
Das fuktioniert dank der sogenannten Dev-Services mit etwas Mehraufwand auch mit einer Persistence, die wir hier noch nicht haben. Via Testcontainers würde automatisch eine Datenbank hochgefahren, das Schema erstellt und ggf. Testdaten eingespielt werden.  
(test speichern)
Quarkus hat erkannt, dass sich ein Test geändert hat und führt ihn neu aus.
(quarkus ext ls)
Features bzw. die meisten Dependencies werden als Extension gemananged. Extensions sind Kompomenten, die für Quarkus, d.h. u.a. für das native Compilieren via GaalVM optimiert sind und sich in die Konfiguration von Quarkus einfügen. 

* [ ] TODO Kann die Folie 'Quarkus vs. JEE / Microprofile' evtl. weg? Das wurde vorher schon alles gesagt.

-> Slide: CDI (ArC)

-> Matt:
Zwei Besonderheiten möchte ich noch erwähnen.
Das eine ist die CDI-Implementierung.
Quarkus führt das Annotation-Scanning zur Compilezeit durch und lässt Reflections nur unter eingeschränkt zu. Dadurch ist es möglich dass ungenutzte Beans gar nicht erst in das Deploy-Artefakt gelangen.
Klassen werden erst instanziiert, wenn sie benötigt werden.
Die Inject-Annotation kann entfallen, wenn das Feld durch eine andere Annotation z.B. ConfigProperty oder RestClient annotiert ist und dadurch klar ist, dass Injection stattfinden soll
No-Args-Constructor kann für CDI-Beans entfallen. Gibt es genau einen Constructor, kann die Inject-Annotatin auch entfallen.

* [ ] TODO Folie 'Dev Mode' kann entfallen? - Wurde schon gezeigt

-> Matt:
Quarkus Persistence.
Quarkus nutzt Hibernate mit Panache. Panache ermöglicht vereinfachte HQL-Abfragen.
Es wird das Repository-Pattern unterstützt. Dabei implementiert das Repository das Interface PanacheRepository<T>.
Oder das Active Record Pattern, bei dem die Datenbank-Entity selbst von PanacheEntity abgeleitet wird. Das Entity implementiert dann selbst Methoden für die Datenbankoperationen.


* [ ] TODO Folie 'Quarkus Test' kann entfallen? - Wurde schon gezeigt

* [ ] TODO Folie 'Reactive' kann entfallen? - Hatte Stefan das schon?

-> Slide: Reactive und Quarkus

-> Matt:

Bisher war Quarkus noch kein Stück reactive, oder doch?
Quarkus ist im Manschinenraum reactive.

* [ ] Dazu noch ein bisschen mehr

