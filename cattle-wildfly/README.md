# Cattle Reactive Wildfly

## Build and deploy
```shell
mvn clean package
docker compose up --build
```

## Requests

```shell
curl -iX POST http://localhost:8080/cattle --header "Content-Type: application/json" --data '{"lom": 1, "birthday": "1940-03-10"}'
```

```shell
curl -iX GET http://localhost:8080/cattle/1
```
