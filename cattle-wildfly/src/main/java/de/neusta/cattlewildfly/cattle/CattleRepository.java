package de.neusta.cattlewildfly.cattle;


import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

@ApplicationScoped
public class CattleRepository {

  @Inject
  private EntityManager em;

  public Cattle findById(Long id) {
    return em.find(Cattle.class, id);
  }

  public void persist(Cattle cattle) {
    em.persist(cattle);
  }

  public Cattle findByLom(Long lom) {
    return em.createQuery("select c from Cattle c where c.lom = :lom", Cattle.class).setParameter("lom", lom)
      .getSingleResult();
  }
}
