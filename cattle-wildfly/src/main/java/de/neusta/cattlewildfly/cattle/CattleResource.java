package de.neusta.cattlewildfly.cattle;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("cattle")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
public class CattleResource {
  @Context
  UriInfo uriInfo;
  @Inject
  CattleService cattleService;

  @GET
  @Path("{lom}")
  @Produces(MediaType.APPLICATION_JSON)
  public CattleDto byLom(@PathParam("lom") Long lom) {
    return cattleService.findCattle(lom);
  }

  @POST
  @Path("")
  @Consumes(MediaType.APPLICATION_JSON)
  @Transactional
  public Response create(CattleDto cattleDto) {
    cattleService.createCattle(cattleDto);
    return Response.created(
      uriInfo.getBaseUriBuilder().path(CattleResource.class).path(CattleResource.class, "byLom").build(cattleDto.lom())).build();
  }

}
