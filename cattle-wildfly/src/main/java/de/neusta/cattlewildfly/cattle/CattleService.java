package de.neusta.cattlewildfly.cattle;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.NoResultException;

@ApplicationScoped
public class CattleService {

  @Inject
  CattleRepository cattleRepository;

  CattleDto findCattle(Long lom) {

    Cattle cattle;
    try {
      cattle = cattleRepository.findByLom(lom);
    } catch (NoResultException e) {
      return null;
    }
    return CattleDto.from(cattle);
  }

  public void createCattle(CattleDto cattleDto) {
    cattleRepository.persist(cattleDto.toCattle());
  }
}
