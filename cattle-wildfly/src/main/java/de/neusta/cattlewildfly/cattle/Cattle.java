package de.neusta.cattlewildfly.cattle;

import de.neusta.cattlewildfly.EntityBase;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Cattle extends EntityBase {

  @NotNull
  private Long lom;
  @Past
  private LocalDate birthday;
  @ManyToOne
  private Cattle dam;
  @ManyToOne
  private Cattle sire;

}
