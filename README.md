# Quarkus Reactive in Action

Vortrag und Demo-Projekt für die Neusta-Campus-Konferenz.

## Slides

Die Slides können mit [marp](https://github.com/marp-team/marp-cli) von MD in HTML übersetzt werden.

```shell
cd doc
marp --html slides.md
```

## Run in Dev-Mode

```shell
quarkus dev
```

## Run in Production as JAR

```shell
mvn package
```

```shell
docker run -it --rm=true --name quarkus -e POSTGRES_USER=quarkus -e POSTGRES_PASSWORD=quarkus -e POSTGRES_DB=quarkus -p 5432:5432 postgres:16
```

```shell
java -jar ./target/quarkus-app/quarkus-run.jar
```

## Local Docker Compose

```shell
mvn clean install -DskipTests && docker compose up
```

## Performance-Messung

* Resourcen überwachen: `docker stats`
* ggf. Resourcen in `docker-compose.yml` anpassen

Install K6: https://k6.io/docs/get-started/installation/

run 
```shell
k6 run k6/script.js
```


