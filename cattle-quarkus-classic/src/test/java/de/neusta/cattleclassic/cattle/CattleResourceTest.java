package de.neusta.cattleclassic.cattle;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestHTTPEndpoint(CattleResource.class)
class CattleResourceTest {

  @ParameterizedTest
  @MethodSource("provideCattles")
  void test(Long lom, int expectedStatusCode, String expectedBody) {
    when().get(String.valueOf(lom))
      .then().statusCode(expectedStatusCode)
      .body(is(expectedBody));
  }

  private static Stream<Arguments> provideCattles() {
    return Stream.of(
      Arguments.of(1L, 204, ""),
      // cattle from 'import_test.sql'
      Arguments.of(276000400000001L, 200, "{\"birthday\":\"1940-03-10\",\"lom\":276000400000001}")
    );
  }
}