package de.neusta.cattleclassic;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public class EntityBase {
  @Id
  @GeneratedValue
  public Long id;
}
