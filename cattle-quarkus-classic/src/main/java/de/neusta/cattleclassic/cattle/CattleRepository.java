package de.neusta.cattleclassic.cattle;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CattleRepository implements PanacheRepository<Cattle> {
}
