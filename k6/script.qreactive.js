import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  // virtual users to run concurrently.
  vus: 1000,
  // total duration of the test run.
  // duration: '20s',
  iterations: 20*1000,
};

const port = 3102

// The function that defines VU logic.
//
// See https://grafana.com/docs/k6/latest/examples/get-started-with-k6/ to learn more
// about authoring k6 scripts.
//
export default function() {
  let lom = Math.floor(Math.random() * 1000)
  http.get('http://localhost:' + port + '/cattle/' + lom);
  sleep(1);
}
