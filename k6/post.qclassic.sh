#!/bin/bash

port=3101

generate_random_birthday() {
    local year=$((RANDOM % 100 + 1920))  # Random year between 1920 and 2020
    local month=$((RANDOM % 12 + 1))     # Random month between 1 and 12
    local day=$((RANDOM % 28 + 1))       # Random day between 1 and 28 (simplified for all months)
    printf "%04d-%02d-%02d" $year $month $day
}

script_start_time=$(date +%s%N)

for lom in {1..1000}; do
    birthday=$(generate_random_birthday)
    
    curl -X POST http://localhost:$port/cattle \
        --header "Content-Type: application/json" \
        --data '{"lom": '"$lom"', "birthday": "'"$birthday"'"}' &
done

# The '&' makes the function run in the background
# wait for it
wait

script_end_time=$(date +%s%N)
total_elapsed_time=$(( (script_end_time - script_start_time) / 1000000 ))
echo "Total runtime: ${total_elapsed_time} ms"
